**Repositório do Exercícios de MySQL do Prof. Jezmael**

Muito bem, está aqui os arquivos da página que fiz para o exercício da parcial 2 da disciplina de Programação Web.

---

## Instruções de Uso

Você deve utilizar esses arquivos dentro da pasta do Apache, não esqueça disso.

1. Faça o download dos **Arquivos** no Repositório.
2. Descompacte dentro da pasta do seu servidor Apache.
3. Abra seu PHPMyAdmin para **Importar** a base de Dados.
4. Antes de **Importar** crie uma base de dados chamada 'parcial2'.
5. Depois de criada a base, clique em **Importar**, clique no botão **Escolher Arquivo** e procure o arquivo **base.sql**. Clique no botão **Executar** para fazer a importação.
6. Inicie o Apache e entre no endereço correto em seu **Navegador**.

---