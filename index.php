<?php
require('conexao.php');


?>
<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Exercício MySQL - Prof. Jezmael</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <style media="screen">

    </style>
  </head>
  <body>

<main class="container-fluid mt-3">

<div class="row">
  <div class="col-12">
    <h2 class="text-center">Diversas - Desenvolvimento Web</h2>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
  Exibir Questões
</button>

    <hr>
  </div>
</div>
  <div class="row">
    <div class="col-6 text-center">
    <h4 class="text-info font-weight-bold">Tabela "cd"</h4>
    <table class="table table-sm table-striped table-bordered">
      <thead>
        <tr>
          <th>id_cd</th><th>nome</th><th>data_compra</th><th>valor_pago</th>
          <th>local_compra</th><th>album</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $getCDs = $pdo->query("SELECT * FROM cd");
        //$getCDs->execute();
        $todosCDs = $getCDs->fetchAll();
        foreach ($todosCDs as $cd) {
          ?>
          <tr>
            <td><?=$cd->id_cd?></td>
            <td><?=$cd->nome?></td><td><?=$cd->data_compra?></td>
            <td><?=$cd->valor_pago?></td><td><?=$cd->local_compra?></td>
            <td><?=$cd->album?></td>
          </tr>
        <?php }
         ?>
      </tbody>
    </table>
    </div>
    <div class="col-6 text-center">
    <h4 class="text-info font-weight-bold">Tabela "musica"</h4>
    <table class="table table-sm table-striped table-bordered">
      <thead>
        <tr>
          <th>id_musica</th><th>id_cd</th><th>numero</th><th>nome</th>
          <th>artista</th><th>tempo</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $getMusicas = $pdo->query("SELECT * FROM musica WHERE 1");
        $getMusicas->execute();
        $todasMusicas = $getMusicas->fetchAll();
        foreach ($todasMusicas as $musica) {
          ?>
          <tr>
            <td><?=$musica->id_musica?></td>
            <td><?=$musica->id_cd?></td><td><?=$musica->numero?></td>
            <td><?=$musica->nome?></td><td><?=$musica->artista?></td>
            <td><?=$musica->tempo?></td>
          </tr>
        <?php }
         ?>
      </tbody>
    </table>
    </div>
  </div>
<hr>
  <div class="row">
<div class="col-12 text-center">
  <h3>Consultas SQL</h3>
  <form class="form-inline w-75 my-0 mx-auto text-center" action="" method="post">
    <div class="form-group mx-sm-3 mb-2 w-75">
    <input type="text" name="query" class="form-control w-100" id="" placeholder="SELECT * FROM musica" value="<?=(isset($_POST['query'])) ? $_POST['query'] : '';?>">
  </div>
  <button type="submit" name="consultar" class="btn btn-primary mb-2">Enviar</button>
  </form>
</div>
  </div>

  <?php
if(isset($_POST['consultar'])){
  $query = filter_input(INPUT_POST,'query');

  ?>

<?php
if(!empty($query)){
  $getQuery = $pdo->query($query);
  $getQuery->execute();
  $totalLinhas = $getQuery->rowCount();

  if($getQuery->rowCount() > 0){
    $allResult = $getQuery->fetchAll();

  }
  ?>
  <h3 class="text-center text-primary">Resultado (<?=$totalLinhas?>)</h3>
  <div class="table-responsive">
    <table class="table table-sm table-dark table-hover table-striped table-bordered">
      <thead>
        <tr>
          <?php

          $totalColunas = count((array) $allResult[0]);
          foreach($allResult[0] as $key => $value){
            echo "<th>".$key."</th>";
          }
          ?>
        </tr>
      </thead>
      <tbody>

          <?php
          foreach ($allResult as $row) {
            $linha = (array) $row;
            echo "<tr>";
            foreach ($linha as $key => $value) {
              echo "<td>".$value."</td>";
            }
            echo "</tr>";
          }
          ?>

      </tbody>
    </table>
  </div>
  <?php
}else{
  echo "Query vazia";
}

 ?>


<?php } ?>


<div class="row">
  <div class="col-6">
  <h3>Inserir Dados</h3>

<?php
if(isset($_POST['novo'])){

  $nome = filter_input(INPUT_POST,'nome');
  $data = filter_input(INPUT_POST,'data_compra');
  $valor = filter_input(INPUT_POST,'valor_pago');
  $local = filter_input(INPUT_POST,'local_compra');
  $album = filter_input(INPUT_POST,'album');

  $novoCD = $pdo->prepare("INSERT INTO cd (nome,data_compra,valor_pago,local_compra,album) VALUES (?,?,?,?,?)");
  $dadosCD = array($nome,$data,$valor,$local,$album);

  // vou jogar os dados no execute
  $novoCD->execute($dadosCD);

  if($novoCD->rowCount() > 0){
    echo "Inseriu os dados com sucesso!";
  }else{
    echo "ihhhh deu erro....";
  }

}
?>

<form action="" method="POST">
  <fieldset>
  <div class="form-group">
  <label for="">Nome</label>
  <input type="text" name="nome" class="form-control">
  </div>

  <div class="form-group">
  <label for="">Data da Compra</label>
  <input type="date" name="data_compra" class="form-control">
  </div>

  <div class="form-group">
  <label for="">Valor Pago</label>
  <input type="number" name="valor_pago" step="0.01" class="form-control">
  </div>
  <div class="form-group">
  <label for="">Local de Compra</label>
  <input type="text" name="local_compra" class="form-control">
  </div>
  <div class="form-group">
  <label for="">Álbum</label>
  <input type="number" name="album" class="form-control">
  </div>
  <input type="submit" name="novo" value="Cadastrar" class="btn btn-success">
  </fieldset>
  
</form>

  </div>
  <div class="col-6">
    Excluir Dados
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Questões</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ol>
          <li>Exiba todos os campos e todos os CDs com no máximo 5 resultados.</li>
          <li>Exiba os campos nome e data da compra dos CDs ordenados por nome, na ordem crescente.</li>
          <li>Exiba os campos nome e data da compra dos CDs classificados pelas compras mais recentes.</li>
          <li>Exiba o total que foi gasto com a compra dos CDs.</li>
          <li>Exiba todas as músicas (todos os campos) do CDs de código 1.</li>
          <li>Exiba o nome e o artista de todas músicas cadastradas.</li>
          <li>Exiba o tempo total de músicas cadastradas.</li>
          <li>Exiba o número, nome e tempo das músicas do CD 2 em ordem de número.</li>
          <li>Exiba o tempo total de músicas por CD.</li>
          <li>Exiba a quantidade de músicas cadastradas.</li>
          <li>Exiba a média de duração das músicas cadastradas.</li>
          <li>Exiba a quantidade de CDs cadastrados.</li>
          <li>Exiba o nome das músicas do artista Natiruts</li>
          <li>Exiba a quantidade de músicas por CDs.</li>
          <li>Exiba o nome de todos CDs comprados em Várzea Alegre-CE.</li>
          <li>Exiba o nome do CD e o nome da primeira músicas de todos CDs</li>
          <li>Exiba o nome da música com o maior tempo de duração.</li>
          <li>Exiba o nome da música com menor tempo de duração.</li>
          <li>Exiba o CD que custou mais caro.</li>
          <li>Exiba o CD comprado em Russas que custou mais barato.</li>
        </ol>
      </div>
    </div>
  </div>
</div>

</main>

    <script src="jquery-3.2.1.min.js" charset="utf-8"></script>
    <script src="bootstrap.min.js" charset="utf-8"></script>

  </body>
</html>
