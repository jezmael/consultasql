-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 06/06/2019 às 23:26
-- Versão do servidor: 10.1.40-MariaDB
-- Versão do PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `parcial2`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `cd`
--

CREATE TABLE `cd` (
  `id_cd` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `data_compra` date NOT NULL,
  `valor_pago` float NOT NULL,
  `local_compra` varchar(100) NOT NULL,
  `album` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `cd`
--

INSERT INTO `cd` (`id_cd`, `nome`, `data_compra`, `valor_pago`, `local_compra`, `album`) VALUES
(1, 'Natiruts', '2019-06-01', 19.9, 'Várzea Alegre-CE', 3),
(2, 'Banda do Mar', '2019-06-03', 20.5, 'Russas-CE', 1),
(3, 'Minutes to Midnight', '2019-06-02', 30.5, 'Juazeiro do Norte-CE', 6),
(4, 'Ventura', '2019-05-08', 17.5, 'Russas-CE', 3),
(5, 'Oriente (Acústico)', '2019-05-16', 15.9, 'Várzea Alegre-CE', 3),
(6, '24K Magic', '2019-06-04', 34.5, 'Fortaleza-CE', 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `musica`
--

CREATE TABLE `musica` (
  `id_musica` int(11) NOT NULL,
  `id_cd` int(11) NOT NULL,
  `numero` smallint(6) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `artista` varchar(50) NOT NULL,
  `tempo` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `musica`
--

INSERT INTO `musica` (`id_musica`, `id_cd`, `numero`, `nome`, `artista`, `tempo`) VALUES
(1, 1, 1, 'Meu Reggae é Roots - Ao Vivo', 'Natiruts', '00:04:06'),
(2, 1, 6, 'Quero Ser Feliz Também - Ao Vivo', 'Natiruts', '00:03:19'),
(3, 2, 1, 'Cidade Nova', 'Banda do Mar', '00:04:03'),
(4, 2, 5, 'Pode Ser', 'Banda do Mar', '00:04:00'),
(5, 3, 2, 'Namb', 'Linkin Park', '00:02:39'),
(6, 4, 2, 'In The End', 'Linkin Park', '00:03:19'),
(7, 5, 2, 'White Rabit', 'Linkin Park', '00:01:09'),
(8, 6, 2, 'Juntos em Shalow Now', 'Paula Fernandes', '00:02:17');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `cd`
--
ALTER TABLE `cd`
  ADD PRIMARY KEY (`id_cd`);

--
-- Índices de tabela `musica`
--
ALTER TABLE `musica`
  ADD PRIMARY KEY (`id_musica`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `cd`
--
ALTER TABLE `cd`
  MODIFY `id_cd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `musica`
--
ALTER TABLE `musica`
  MODIFY `id_musica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
